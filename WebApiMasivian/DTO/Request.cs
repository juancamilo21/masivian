﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApiMasivian.DTO
{
    public class Request
    {
        /// <summary>
        /// position 0-36, position 37 is red and the position 38 is black
        /// </summary>
        [Range(0, 38)]
        public int position { get; set; }


        [Range(0.5d, maximum: 10000)]
        public double money { get; set; }

    }
}
