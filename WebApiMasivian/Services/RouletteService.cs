﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiMasivian.Models;
using WebApiMasivian.Repositories;

namespace WebApiMasivian.Services
{
    public class RouletteService : IRouletteService
    {
        private IRouletteRepository rouletteRepository;

        public RouletteService(IRouletteRepository rouletteRepository)
        {
            this.rouletteRepository = rouletteRepository;
        }

        public Roulette create()
        {
            Roulette roulette = new Roulette()
            {
                Id = Guid.NewGuid().ToString().Substring(0,5),
                IsOpen = false,
                OpenedAt = null,
                ClosedAt = null
            };
            rouletteRepository.Save(roulette);
            return roulette;
        }

        public Roulette Find(string Id)
        {
            return rouletteRepository.GetById(Id);
        }

        public Roulette Open(string Id)
        {
            Roulette roulette = rouletteRepository.GetById(Id);
            if (roulette == null)
            {
                throw new ArgumentException("the roulette is not created or the ID is incorrect");
            }

            if (roulette.OpenedAt != null)
            {
                throw new ArgumentException("the roulette you selected is already open");
            }
            roulette.OpenedAt = DateTime.Now;
            roulette.IsOpen = true;
            return rouletteRepository.Update(Id, roulette);
        }

        public Roulette Close(string Id)
        {
            Roulette roulette = rouletteRepository.GetById(Id);
            if (roulette == null)
            {
                throw new ArgumentException("the roulette is not created or the ID is incorrect");
            }
            if (roulette.ClosedAt != null)
            {
                throw new ArgumentException("the roulette you selected is already closed");
            }
            roulette.ClosedAt = DateTime.Now;
            roulette.IsOpen = false;
            return rouletteRepository.Update(Id, roulette);
        }

        public Roulette Bet(string Id, string UserId, int position, double moneyBet)
        {
            if (moneyBet <  1 || moneyBet > 10000)
            {
                throw new  ArgumentException("the bet cannot be less than 0.5 nor greater than 100");
            }
            Roulette roulette = rouletteRepository.GetById(Id);
            if (roulette == null)
            {
                throw new ArgumentException("the roulette is not created or the ID is incorrect");
            }

            if (roulette.IsOpen == false)
            {
                throw new ArgumentException("the roulette is closed");
            }

            double value = 0d;
            roulette.board[position].TryGetValue(UserId, out value);
            roulette.board[position].Remove(UserId + "");
            roulette.board[position].TryAdd(UserId + "", value + moneyBet);

            return rouletteRepository.Update(roulette.Id, roulette);
        }

        public List<Roulette> GetAll()
        {
            return rouletteRepository.GetAll();
        }
    }
}
