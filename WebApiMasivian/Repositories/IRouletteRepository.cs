﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiMasivian.Models;

namespace WebApiMasivian.Repositories
{
    public interface IRouletteRepository 
    {
        public Roulette GetById(string Id);

        public List<Roulette> GetAll();

        public Roulette Update(string Id, Roulette roulette);

        public Roulette Save(Roulette roulette);
    }
}
